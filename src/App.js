import React, { useState, useEffect } from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { gapi } from 'gapi-script';

function App() {
    const [ profile, setProfile ] = useState([]);
    const clientId = '5491748626-1nnq9fdqf3r3gv4dbrqrs8gkk4771vo7.apps.googleusercontent.com';
    useEffect(() => {
        const initClient = () => {
            gapi.client.init({
                clientId: clientId,
                scope: 'openid profile email',
                responseType: "code"
            });
        };
        gapi.load('client:auth2', initClient);
    });

    const onSuccess = (res) => {
      console.log(res);
      // send CODE to API Server
      fetch('http://localhost:5015/user/v1/users/verify-oauth', {
        method: 'post',
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
          "code": res.code
        })
      })
      .then(response => response.json())
      .then(data => console.log(data) );
        
      alert('Login Success')
    };

    const onFailure = (err) => {
        console.log('failed', err);
    };

    return (
        <div>
            <h2>React Google Login</h2>
            <br />
            <br />
            {(
                <GoogleLogin
                    clientId={clientId}
                    responseType="code"
                    buttonText="Sign in with Google"
                    onSuccess={onSuccess}
                    onFailure={onFailure}
                    cookiePolicy={'single_host_origin'}
                    isSignedIn={true}
                />
            )}
        </div>
    );
}
export default App;